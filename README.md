# Tic_Tac_Toe

Classic tic-tac-toe game in the console with Python. Player x and o are chosing alternately where to place their symbols.

![](https://gitlab.com/leon_heinrich/tic_tac_toe/-/raw/master/images/start.jpg)

![](https://gitlab.com/leon_heinrich/tic_tac_toe/-/raw/master/images/place_and_change.jpg)

The game ends if someone gets three symbols in a row...

![](https://gitlab.com/leon_heinrich/tic_tac_toe/-/raw/master/images/player_wins.jpg)

...or if there's a tie:

![](https://gitlab.com/leon_heinrich/tic_tac_toe/-/raw/master/images/tie.jpg)